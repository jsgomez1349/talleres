package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

import estructuras.MapNode;
import mundo.FlightMap;

public class FlightRouting {

	public static void main(String[] args) throws Exception{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		boolean seguir = true;
		FlightMap mapa = new FlightMap();
		while (seguir){
			System.out.println("                   __|__            |            __|__");
			System.out.println("                *---oOo---*      ---|---      *---oOo---*");
			System.out.println("                                  _(O)_");
			System.out.println("                       *---------(|_*_|)---------*");
			System.out.println("Bienvenido al sistema de vuelos del mundo");
			System.out.println("Seleccione una opción: ");
			System.out.println("1. Encontrar la ruta más corta entre 2 aeropuertos");
			System.out.println("2. (BONO) Encontrar los aerupuertos a los que puede llegar desde un aeropuerto de origen");

			try{
				String input = br.readLine();

				int opt = Integer.parseInt(input);
				if (opt>2 || opt <1){
					System.out.println("La opción ingresada no existe, pruebe con otra");
				}

				switch (opt) {
				case 1:
					System.out.println("Ingrese el id del Aeropuerto de origen: ");
					String idFrom = br.readLine();
					int from = Integer.parseInt(idFrom);
					System.out.println("Ingrese el id del Aeropuerto de destino: ");
					String idTo = br.readLine();
					int to = Integer.parseInt(idTo);
					Iterable<String> iterable = mapa.getShortestPath(from, to);

					Iterator<String> iter = iterable.iterator();

					if(iter.hasNext()){
						System.out.println("La ruta más corta entre el aeropuerto "+ from+" hasta el aeropuerto "+ to +" es:");
						while(iter.hasNext())
						{
							System.out.println(iter.next());
						}
					}else{
						System.out.println("No se encontró una ruta entre el aeropuerto "+from +" y el aeropuerto "+to);
					}
					break;
				case 2:
					System.out.println("Ingrese el id del Aeropuerto de origen: ");
					String id = br.readLine();
					int idi = Integer.parseInt(id);
					Iterable<String> iterable1 = mapa.getDFS(idi);
					if(iterable1!=null){
						Iterator<String> iter1 = iterable1.iterator();
						
						while(iter1.hasNext())
						{
							System.out.println(iter1.next());
						}
						
					}else{
						System.out.println("No se encontró el aeropuerto de origen.");
					}
					break;

				default:
					System.out.println("Opción no reconocida, intente de nuevo.");;
				}
			}catch (NumberFormatException num){
				System.out.println("Opción no númerica, ingrese un número");
			}
		}

	}

}
