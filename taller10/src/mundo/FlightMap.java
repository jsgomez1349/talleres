package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import estructuras.MapArbolRecubrimiento;
import estructuras.MapDijkstraSP;
import estructuras.MapEdge;
import estructuras.MapGraph;
import estructuras.MapNode;

public class FlightMap {

	/**
	 * Archivo que contiene la información de los nodos que corresponden a los
	 * aeropuertos del mundo.
	 */
	private final static String NODES_FILE = "./data/nodes.csv";

	/**
	 * Constante que establece la ubicación del archivo kml que se produce al calcular la
	 * ruta entre dos puntos del grafo
	 */
	private static final String OUTPUT_FILE = "./data/output.kml";



	/**
	 * Archivo que contiene la información de las aerolíneas.
	 */
	private final static String NAMES_FILE = "./data/names.csv";

	/**
	 * Archivo que contiene la información de los arcos que corresponden a los vuelos
	 * existentes entre los aeropuertos.
	 */
	private final static String EDGES_FILE = "./data/edges.csv";

	/**
	 * Grafo correspondiente a los vuelos, aerolíneas y aeropuertos.
	 */
	private MapGraph graph;

	private Map<Integer, String> airlines;


	/**
	 * Constructor principal de la clase, esta se encuentra encargada de realizar la
	 * inicialización y carga de los archivos de descripción de la representación de
	 * grafo de los aeropuertos, vuelos y aeroíneas.
	 */
	public FlightMap()
	{
		graph = new MapGraph();
		airlines = new HashMap<Integer, String>();

		//Nodos
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(NODES_FILE));
			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{
				try{
					String[] values = line.split(",");
					graph.addNode(Integer.parseInt(values[0]), Double.parseDouble(values[6]), Double.parseDouble(values[7]), values[1], values[2], values[3], values[4].replaceAll("\"", ""));
				}catch(Exception e){

				}

			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de nodos: " + e.getMessage());
		}

		//Arcos
		try
		{
			br = new BufferedReader(new FileReader(EDGES_FILE));
			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{        
				try{
					String[] values = line.split(",");
					graph.addEdge(Integer.parseInt(values[3]), Integer.parseInt(values[5]), Integer.parseInt(values[1]), Double.parseDouble(values[8]) );
				}catch(Exception e){

				}
			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de arcos: " + e.getMessage());
		}

		//Airlines
		try
		{
			br = new BufferedReader(new FileReader(NAMES_FILE));
			String line;
			br.readLine();
			while((line = br.readLine()) != null)
			{        
				try{
					String[] values = line.split(",");
					airlines.put(Integer.parseInt(values[0]), values[1]);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			br.close();
		}
		catch(Exception e)
		{
			System.out.println("Error al leer el archivo de nombres: " + e.getMessage());
		}
	}

	/**
	 * P1) Busca un Aeropuerto dado un id
	 * @param id - El id del Aeropuerto
	 * @return El Aeropuerto buscado, null si no existe.
	 */
	public MapNode getNodeById(int id){
		return graph.getMapNode(id);
	}

	/**
	 * P2) Busca un Aeropuerto dado su identificador IATA
	 * @param IATA - El código IATA del Aeropuerto
	 * @return El Aeropuerto buscado, null si no existe.
	 */
	public MapNode getNodeByIATA(String IATA){
		return graph.getNodeByIATA(IATA);
	}

	/**
	 * P3) Devuelve los códigos IATA de todos los aeropuertos del mapa.
	 * @return Una lista con todos los códigos encontrados.
	 */
	public List<String> getAllIATACodes(){
		List<String> response = new ArrayList<String>();

		Iterator<MapNode> iter = graph.iterator();
		while (iter.hasNext()){
			MapNode node = iter.next();
			response.add(node.IATA);
		}

		return response;
	}

	/**
	 * BONO) Busca un vuelo entre dos Aeropuertos dados sus id's
	 * @param from - id del Aeropuerto de origen
	 * @param to - id del Aeropuerto de destino
	 * @return El vuelo entre los aeropuertos, null si no existe.
	 */
	public String getFlight(int from, int to ){

		Set<MapEdge> edges = graph.getEdgesFromNode(from);
		if (edges == null)
			return null;
		for (MapEdge edge : edges){
			if (edge.to == to){
				return edge.toString(airlines.get(edge.airline));
			}
		}
		return null;
	}

	/**
	 * Retorna una estructura iterable de Strings con las direcciones para ir del nodo idSource al nodo idDestination
	 * @param idSource int El identificador de la fuente
	 * @param idDestination int El identificador del destino
	 * @return resp Una estrucutra iterable con las direcciones
	 */
	public Iterable<String> getShortestPath(int idSource, int idDestination)
	{

		MapDijkstraSP mapDijktraSP = new MapDijkstraSP(graph, idSource, idDestination);
		LinkedList<MapEdge> path = mapDijktraSP.pathTo();
		LinkedList<String> resp = new LinkedList<>();

		if(path!=null){
			
			for (MapEdge mapEdge : path)
			{
				resp.add("Desde: "+graph.getMapNode(mapEdge.from).toString()+", " +
						"Hasta: "+graph.getMapNode(mapEdge.to)+", Por "+mapEdge.distance/1000+" kilometros");
			}

			getShortestPathFile(idSource, idDestination);
		}

		return resp;
	}

	/**
	 * Retorna una estructura iterable de Strings con los nodos que se encuentran 
	 * al nivel ingresado por parametro del nodo origen
	 * @param idSource int El identificador de la fuente
	 * @param nivel int Nivel que se va a buscar
	 * @return resp Una estrucutra iterable con los nodos
	 */
	public Iterable<String> getDFS(int idSource)
	{
		MapArbolRecubrimiento mapDFS = new MapArbolRecubrimiento(graph, idSource);
		if(graph.getMapNode(idSource)!=null){
			ArrayList<MapNode> nodes = mapDFS.getAnswer();
			System.out.println("La cantidad de aeropuertos a los que puede llegar es "+nodes.size());
			LinkedList<String> respi = new LinkedList<>();
			for(int i=0; i< nodes.size(); i++)
			{
				respi.add(nodes.get(i)+"");
			}
			return respi;
		}
		return null;
	}

	/**
	 * Escribe el archivo kml de salida con la información de la ruta más corta del nodo idSource al nodo idDestination
	 * Este archivo se puede importar a google maps para visualizar la ruta
	 * @param idSource int El identificador de la fuente
	 * @param idDestination int El identificador del destino
	 */
	private void getShortestPathFile(int idSource, int idDestination)  {
		File f = new File(OUTPUT_FILE);
		try {

			if(!f.exists()) {
				f.createNewFile();
			}

			FileWriter fw = new FileWriter(f);
			PrintWriter pw = new PrintWriter(fw);

			writeKMLHeader(pw);
			MapDijkstraSP mapDijkstraSP = new MapDijkstraSP(graph, idSource, idDestination);
			LinkedList<MapEdge> path = mapDijkstraSP.pathTo();
			if(path!=null){
				
				for (MapEdge mapEdge : path)
				{
					pw.println("        " + graph.getMapNode(mapEdge.from).lng + ", " + graph.getMapNode(mapEdge.from).lat);
				}
				
				pw.println("        " + graph.getMapNode(idDestination).lng + ", " + graph.getMapNode(idDestination).lat);
				writeKMLFooter(pw);
				fw.flush();
				fw.close();


			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Escribe el encabezado de un archivo kml
	 * @param pw El PrintWriter que escribe al archivo
	 * @throws IOException En caso de que haya un error a la hora de escribir en el archivo
	 */
	private void writeKMLHeader(PrintWriter pw) throws IOException
	{

		pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		pw.println("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
		pw.println("  <Document>");
		pw.println("    <name>Ruta más corta</name>");
		pw.println("    <Placemark>");
		pw.println("      <name>Ruta más corta</name>");
		pw.println("      <LineString>");
		pw.println("      <altitudeMode>absolute</altitudeMode>");
		pw.println("      <coordinates>");
	}

	/**
	 * Escribe el pie de pagina de un archivo kml
	 * @param pw El PrintWriter que escribe al archivo
	 * @throws IOException En caso de que haya un error a la hora de escribir en el archivo
	 */
	private void writeKMLFooter(PrintWriter pw) throws IOException
	{
		pw.println("      </coordinates>");
		pw.println("      </LineString>");
		pw.println("    </Placemark>");
		pw.println("  </Document>");
		pw.println("</kml>");

	}

}


